package se1;

import java.util.Date;

/**
 * Academic Event
 * 
 * This class represents an event that contributes to an academic subject.
 * It extends Event.
 * 
 * @author SE1
 */
public abstract class AcademicEvent extends Event{
    
    private double weight;//Weight of event on a module
    private Milestone[] milestones; //milestones linked to a event
    private Task[] task;//Tasks linked to a event



    /**
     * 
     * @param title = to set String title 
     * @param organsier = to set Profile organsier
     * @param startTime = to set Date startTime
     * @param endTime = to set Date endTime
     * @param notes = to set String notes 
     * @param moduleId = to set int moduleId 
     * @param setWeight = to set int weight
     * @param setMilestones = to set milestones 
     * @param setTask = to set Task
     */

    
    public AcademicEvent(String title, Profile organsier, Date startTime, Date endTime, String notes, String moduleId,

            double setWeight,Milestone[] setMilestones, Task[] setTask) {
        
        super(title, organsier, startTime, endTime, notes, moduleId);
        weight = setWeight;
        milestones = setMilestones;
        task = setTask;
    }
    /**
    * 
    * @param title = to set String title 
    * @param organsier = to set Profile organsier
    * @param startTime = to set Date startTime
    * @param endTime = to set Date endTime
    * @param moduleId = to set int moduleId 
    * @param setWeight = to set int weight
    * @param setMilestones = to set milestones 
    */
     public AcademicEvent(String title, Profile organsier, Date startTime, Date endTime,  
            String moduleId,double setWeight,Milestone[] setMilestones ) {
        super(title, organsier, startTime, endTime, moduleId);
        weight = setWeight;
        milestones = setMilestones;
        
    }
    /**
     * Returns weight
     * @return 
     */
    public Double getWeight() {
        return weight;
    }
    /**
     * Returns Milestone
     * @return 
     */
    public Milestone[] getMilestones() {
        return milestones;
    }
    /**
     * Returns Task array
     * @return 
     */
    public Task[] getTask() {
        return task;
    }

    public String[] getNotes(){
        String[] temp = new String[1];
        temp[0] = "Note";
        return temp;
    }

    @Override
    public String toString() {
        return "AcademicEvent{" + "Weight=" + weight + ", milestones=" + milestones + ", task=" + task + ',' + super.toString() + '}';
    }
    
}

