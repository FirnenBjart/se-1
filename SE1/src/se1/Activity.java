package se1;

import java.util.ArrayList;
import se1.Controllers.DatabaseController;

/**
 * Activity
 * 
 * This class represents an activity that contributes to the success of a Task.
 *
 * @author SE1
 */
public class Activity {
    int id; // Id of the activity
    private String title; //title of the activity 
    private int timeSpent; // time spent in mintues on the task 
    private String notes; // notes about the activity 
    boolean isDone; // is activity done
    
    public Activity() {
    }
    
    /**
     * @param setTitle = sets title for activity
     * @param timeSpent = sets timespent for activity 
     */
    public Activity(String setTitle, int timeSpent){
        notes = new String(); 
        this.timeSpent = timeSpent;
        this.title = setTitle;
    }
    
    public Activity(int id, String setTitle, int timeSpent, String notes, boolean isDone) {
        this.id = id;
        this.timeSpent = timeSpent;
        this.title = setTitle;
        this.isDone = isDone;
    }
            
    public String getNotes() {
        return notes;
    }
    /**
    * Returns the bool isDone
    * @return 
    */
    public boolean IsDone() {
        return isDone;
    }

    /**
     * updates isDone
     */
    public void updateDone(){
        isDone = DatabaseController.updateActiveBool(id);
    }
    /**
     * Add a note to Notes
     * @param toAdd string to add
     * @return true if added succesfully false if not
     */

    public boolean addNotes(String[] toAdd) {
        try{
        StringBuilder temp = new StringBuilder(notes);
        for (String o : toAdd){
            temp.append(o);
            temp.append("\n");
        }
        return true;}
        catch(Exception e){
            return false;
        }
    }
       
  
    /**
     * Removes a note a position toRemove in notes array 
     * @param toRemove index to remove at
     * @return true if successful false otherwise
     */
    public boolean removeNotes(int toRemove){
        try{
        String[]temp=notes.split("\n");
        ArrayList<String> toRemovee = new ArrayList<String>() ;
        for(String o : temp){
            toRemovee.add(o);
        }
        toRemovee.remove(toRemove);
        StringBuilder tempp = new StringBuilder();
        for(String o : toRemovee){
            tempp.append(o);
        }
        notes= tempp.toString();
        return true;
        }
        catch(Exception e){
            return false;
        }
    }
    
    public Activity(int id) {
        Activity temp = DatabaseController.getActivity(id);
        this.id = temp.id;
        this.timeSpent = temp.timeSpent;
        this.title = temp.title;
        this.isDone = temp.isDone;
    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getTimeSpent() {
        return timeSpent;
    }

    /**
     * 
     * @param note
     * @return 
     */
    public boolean addNotes(String note) {
        try{
        this.notes = this.notes+note+ "/n";
      return true;}
        catch(Exception e){
            return false;
        }
    }

    /**
     * sets id 
     * @param id to be set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Activity{" + "title=" + title + ", timeSpent=" + timeSpent + ", notes=" + notes + ", isDone=" + isDone +'}';
    }

}
