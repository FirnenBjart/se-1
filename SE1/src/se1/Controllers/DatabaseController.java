/*
 * 
 */
package se1.Controllers;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import se1.Milestone;
import se1.Profile;
import se1.Task;

/**
 *
 * @author SE1
 */
public class DatabaseController {

    static Connection c = null;
    /**
     * Conects to the database
     */
    public DatabaseController() {
        connect();
    }
    /**
     * checks connection to the database
     * @return if database is online
     */
    private static boolean checkConnection() {
        try {
            return !c.isClosed();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    /**
    * connects to the database from the controller
    */
    private static void connect() {
        try {
            Properties user = new Properties();
            FileInputStream input = new FileInputStream("src/se1/Controllers/config.properties"); //Ask for config file not on git
            user.load(input);
            c = DriverManager.getConnection("jdbc:postgresql://softwareengineerings1.ceh9kkep6klc.eu-west-1.rds.amazonaws.com/SE1", user);
            System.out.println("Database Opened: " + !c.isClosed());
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * close the link to the database
     */
    public static void close() {
        try {
            c.close();
            System.out.println("Database Closed: " + c.isClosed());
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }

    /**
     * Method to search the database for a username
     *
     * @param toSearchFor
     * @return true if username is found
     */
    public static boolean seachUsers(int toSearchFor) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT profile.id FROM database.profile WHERE profile.id = ?");
            stmt.setInt(1, toSearchFor);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return (rs.getInt("id") == toSearchFor);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * Fetchs for the database a encrypt password from one password
     *
     * @param username name of profile to fetch data from
     * @return encrypted password for user
     */
    public static String getEncryptPassword(int username) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT profile.passwordhash FROM database.profile WHERE profile.id = ?");
            stmt.setInt(1, username);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            String hash = rs.getString("passwordhash");
            hash = hash.replaceAll("\\s+", "");
            return se1.Controllers.EncryptionController.getMD5(hash);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param toEncrypt String to be Encrypted
     * @return encrypted String
     */
    public static String encrypt(String toEncrypt) {
        return se1.Controllers.EncryptionController.getMD5(toEncrypt);
    }

    /**
     * Fetch a profile for use in the rest of the program
     *
     * @param username denotes profile to get
     * @return no username not found return null
     */
    public static Profile fetchProfile(int username) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT profile.name, profile.id, profile.email FROM database.profile WHERE profile.id = ?");
            stmt.setInt(1, username);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return new Profile(rs.getString("name"), rs.getInt("id"), rs.getString("email"));
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     *Gets tasks from the dataBase
     * @param activity activity to search by 
     * @return a arraylist of tasks fetched
     */
    public static ArrayList<se1.Task> getTasks(int activity) {
        ArrayList<se1.Task> tasks = new ArrayList();
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT task.id, milestonetaskbridge.taskid , task.title, task.notes, task.isDone, event.typeofobject FROM database.task, database.milestonetaskbridge, database.event, database.milestone WHERE milestonetaskbridge.milestoneid = ? AND milestone.id = ? AND milestonetaskbridge.taskid = task.id AND event.eventid = milestone.eventid");
            stmt.setInt(1, se1.Controllers.DatabaseController.getMilestoneID(activity));
            stmt.setInt(2, se1.Controllers.DatabaseController.getMilestoneID(activity));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                //put in notes from database string
                se1.Task temp = new se1.Task(rs.getInt("id"), rs.getString("title"), rs.getString("notes"), false);
                System.out.println(temp);
                tasks.add(temp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tasks;
    }
    
    /**
     * gets activity 
     * @param activityID Id of activity to get
     * @return activity fetched or null
     */
    public static se1.Activity getActivity(int activityID) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT activity.id, activity.title, activity.timespent, activity.notes, activity.isdone FROM database.activity WHERE activity.id = ?");
            stmt.setInt(1, activityID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return new se1.Activity(rs.getInt("id"), rs.getString("title"), rs.getInt("timespent"), rs.getString("notes"), rs.getBoolean("isdone"));
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     * gets Activites linked to to a taskId
     * @param taskId task id to search for
     * @return array list of Activites fetched
     */
    public static ArrayList<se1.Activity> getActivites(int taskId) {
        ArrayList<se1.Activity> activites = new ArrayList();
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT activity.id, activity.title, activity.timespent, activity.notes, activity.isdone FROM database.activity, database.task WHERE task.id = ? AND activity.id = task.id");
            stmt.setInt(1, taskId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                se1.Activity tempActivity = new se1.Activity(rs.getInt("id"), rs.getString("title"), rs.getInt("timespent"), rs.getString("notes"), rs.getBoolean("isdone"));
                activites.add(tempActivity);
            }
            return activites;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return activites;
    }

    /**
     * 
     * @param toSave
     * @return
     */
    public static boolean saveProfile(Profile toSave) {
        //save a profile to the database
        return false;
    }
    /**
     * gets profile from the database
     * @param username of profile you are trying to fetch 
     * @return profile fetched
     */
    public static se1.Profile getProfile(int username) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT profile.name, profile.email FROM database.profile WHERE profile.id = ?");
            stmt.setInt(1, username);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            se1.Profile temp = new se1.Profile(rs.getString("name"), username, rs.getString("email"));
            return temp;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * get milestones linked to a username
     * @param username username to search by
     * @return arraylist of milestones
     */
    public static Date getStartTime(int eventID) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT event.starttime FROM database.event WHERE event.eventid = ?");
            stmt.setInt(1, eventID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return convertStringtoDate(String.format("%10s", rs.getString("starttime")).replace(' ', '0'));
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Date getEndTime(int eventID) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT event.endtime FROM database.event WHERE event.eventid = ?");
            stmt.setInt(1, eventID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return convertStringtoDate(String.format("%10s", rs.getString("endtime")).replace(' ', '0'));
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void addProfile(se1.Profile profile, String password) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("INSERT INTO database.profile (id, name, email,accesslevel, \"School ID\", courseId, passwordhash) VALUES (?,?,?,?,?,?,?)");
            stmt.setInt(1, profile.getUniversityID());
            stmt.setString(2,profile.getName());
            stmt.setString(3, profile.getEmail());
            stmt.setInt(4,1);
            stmt.setInt(5,1);
            stmt.setInt(6,1);
            stmt.setString(7,password);
            stmt.executeUpdate();
            c.commit();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    /**
     * get milestones linked to a username
     * @param username username to search by
     * @return arraylist of milestones
     */
    public static ArrayList<se1.Milestone> getMilestones(int username) {
        ArrayList<se1.Milestone> milestones = new ArrayList();
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT milestone.title, milestone.deadline, milestone.notes, event.eventid, event.typeofobject FROM database.milestone, database.event WHERE event.organizerid = ? AND milestone.eventid = event.eventid");
            stmt.setInt(1, username);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String[] temp = new String[10];
                temp[0] = rs.getString("notes");
                se1.Milestone tempMilestone = new se1.Milestone(rs.getString("title"), convertStringtoDate(String.format("%10s", rs.getString("deadline")).replace(' ', '0')), temp, rs.getInt("eventid"), rs.getInt("typeofobject"));
                milestones.add(tempMilestone);
            }
            return milestones;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return milestones;
    }
    /**
     * adds task linked to a Milestone to database
     * @param task task to add
     * @param stone Milestone to add
     */

    public static int getMilestoneID(int eventID) {

        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT milestone.id FROM database.milestone, database.event WHERE event.eventid = ? AND milestone.eventid = event.eventid");
            stmt.setInt(1, eventID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getInt("id");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * adds task linked to a Milestone to database
     * @param task task to add
     * @param stone Milestone to add
     */
    public static void addTask(se1.Task task, se1.Milestone stone) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("INSERT INTO database.task (id, title, notes, isDone) VALUES (?,?,?,?)");
            stmt.setInt(1, task.getTaskID());
            stmt.setString(2, task.getTitle());
            stmt.setString(3, task.getNotes());
            stmt.setBoolean(4, task.isIsDone());
            stmt.executeUpdate();
            stmt = c.prepareStatement("INSERT INTO database.milestonetaskbridge(milestoneid, taskid) VALUES (?,?)");
            stmt.setInt(1, se1.Controllers.DatabaseController.getMilestoneID(stone.getEventID()));
            stmt.setInt(2, task.getTaskID());
            stmt.executeUpdate();
            c.commit();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * add Milestone to database
     * @param stone Milestone to added
     */
    public static void addMilestone(se1.Milestone stone) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("INSERT INTO database.milestone (id, title, deadline,notes, eventid) VALUES (?,?,?,?,?)");
            stmt.setInt(1, se1.Controllers.DatabaseController.maxMilestoneID() + 1);
            stmt.setString(2, stone.getTitle());
            stmt.setInt(3, Integer.parseInt(convertDatetoString((stone.getDeadline()))));
            stmt.setString(4, stone.getNotes()[0]);
            stmt.setInt(5, stone.getEventID());
            stmt.executeUpdate();
            c.commit();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * get pofileId of organiser of a mileStone
     * @param stone Milestone to search in database
     * @return organizer name
     */
    public static String milestoneUser(se1.Milestone stone) {
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT event.organizerid FROM database.milestone, database.event WHERE milestone.eventid = ? AND milestone.eventid = event.eventid");
            stmt.setInt(1, stone.getEventID());
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getString("organizerid");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    /**
     * get max Task ID in database
     * @return the max Task Id
     */
    public static int maxTaskID() {

        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT MAX(id) FROM database.task");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getInt("max");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
     /**
     * get max Event ID in database
     * @return the max Event Id
     */
    public static int maxEventID() {

        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT MAX(eventid) FROM database.event");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getInt("max");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    /**
     * set isdone for a task in the Database
     * @param taskID task to change
     * @param bool what to change value to 
     */

    public static int maxMilestoneID() {

        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT MAX(id) FROM database.milestone");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getInt("max");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * set isdone for a task in the Database
     * @param taskID task to change
     * @param bool what to change value to 
     */
    public static void setBoolTask(int taskID, boolean bool) {
        System.out.println("TASK ID " + taskID + "BOOL" + bool);
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("UPDATE database.task set isdone=? WHERE task.id = ?");
            stmt.setBoolean(1, bool);
            stmt.setInt(2, taskID);
            stmt.executeUpdate();
            c.commit();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * set isdone for a activity in the Database 
     * @param activityID activity to change
     * @param bool what to change value to 
     */
    public static void setBoolActivity(int activityID, boolean bool) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("UPDATE database.activity set isdone=? WHERE activity.id = ?");
            stmt.setBoolean(1, bool);
            stmt.setInt(2, activityID);
            stmt.executeUpdate();
            c.commit();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * to get isDone of a activtyId
     * @param activityID Id to get isdone in dataBase 
     * @return 
     */
    public static boolean updateActiveBool(int activityID) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("SELECT task.IsDone FROM database.task WHERE task.id = ?");
            stmt.setInt(1, activityID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getBoolean("isdone");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * get eventType from database
     * @param eventID Id of event to get it from
     * @return The type of object
     */
    public static int eventType(int eventID) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("SELECT event.typeofobject FROM database.event WHERE event.eventid = ?");
            stmt.setInt(1, eventID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getInt("typeofobject");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    /**
     * Get the course a user doe from database
     * @param user ID of user 
     * @return course the user does
     */
    public static String getCourse(int user) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("SELECT courseid.name FROM database.courseid, database.profile WHERE profile.id = ? AND courseid.id = profile.courseid");
            stmt.setInt(1, user);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getString("name");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     * Gets modules from a user
     * @param user Id to get modules for
     * @return users Modules String 
     */
    public static String getModules(int user) {
        PreparedStatement stmt = null;
        try {
            stmt = c.prepareStatement("SELECT DISTINCT ON (event.moduleid) event.moduleid FROM database.event, database.profile WHERE profile.id = ? AND profile.id = event.organizerid");
            stmt.setInt(1, user);
            ResultSet rs = stmt.executeQuery();
            String strinHolder = "";
            while (rs.next()) {
                strinHolder = strinHolder + rs.getString("moduleid") + " , ";
            }
            return strinHolder;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Get list of Courseworks related to a user
     * @param username get to get courseWork for
     * @return ArrayList of corsework linked to user
     */
    public static ArrayList<se1.Coursework> getCoursework(int username) {
        ArrayList<se1.Coursework> coursework = new ArrayList();
        PreparedStatement stmt = null;
        PreparedStatement stmt2 = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT event.eventid, event.title, event.organizerid,\n"
                    + "event.starttime, event.endtime, event.notes,\n"
                    + "event.moduleid, event.typeofobject\n"
                    + "FROM database.event, database.coursework\n"
                    + "WHERE event.organizerid = ?\n"
                    + "AND event.eventid = coursework.eventid;");
            stmt.setInt(1, username);
            ResultSet rs = stmt.executeQuery();
            se1.Profile temp = new Profile(username);
            stmt2 = c.prepareStatement("SELECT event.eventid, event.title, event.notes FROM database.event , database.academicEvent WHERE event.eventid = academicEvent.eventid AND event.organizerid = ? ");
            stmt2.setInt(1, username);
            ResultSet rs2 = stmt2.executeQuery();
            while (rs2.next()) {
                se1.Coursework tempCoursework = new se1.Coursework(rs2.getString("title"), temp, convertStringtoDate(String.format("%10s", rs2.getString("starttime")).replace(' ', '0')),
                        convertStringtoDate(String.format("%10s", rs2.getString("endtime")).replace(' ', '0')), rs2.getString("moduleid"), rs2.getDouble("weighting"), new se1.Milestone[0]);
                coursework.add(tempCoursework);
            }
            return coursework;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return coursework;
    }    
      
    /**
     * Get list of TimetableEvent related to a user
     * @param username get to get courseWork for
     * @return ArrayList of TimetableEvent linked to user
     */
    public static ArrayList<se1.TimetableEvent> getEvent(int username) {
        ArrayList<se1.TimetableEvent> events = new ArrayList();
        PreparedStatement stmt = null;
        try {
            c.setAutoCommit(false);
            stmt = c.prepareStatement("SELECT event.title, event.organizerid, event.starttime, event.endtime, event.moduleid FROM database.event WHERE event.organizerid = ?;");
            stmt.setInt(1, username);
            ResultSet rs = stmt.executeQuery();           
            while (rs.next()){              
                String title = rs.getString("title");
                rs.getString("organizerid");
                
                Date d = convertStringtoDate(String.format("%10s", rs.getString("starttime")).replace(' ', '0'));
                Date d2 = convertStringtoDate(String.format("%10s", rs.getString("endtime")).replace(' ', '0'));
                String modID = rs.getString("moduleid");
                se1.Profile temp = new se1.Profile(username);
                
                se1.TimetableEvent tempEvent = new se1.TimetableEvent(title, temp, d, d2, modID);
                events.add(tempEvent);
            }
            return events;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return events;
    }
    
    /**
     * converts a date object to a string
     * @param toConvert Date to be converted
     * @return a 10 dight int in fashion (ddMMYYHHmm)
     */
       private static String convertDatetoString(Date toConvert){
        StringBuilder toReturn = new StringBuilder();
        toReturn.append(toConvert.getDate());
        toReturn.append(toConvert.getMonth());
        toReturn.append(toConvert.getYear());
        toReturn.append(toConvert.getHours());
        toReturn.append(toConvert.getMinutes());
        return toReturn.toString();
    }
    /**
     * converts string to a date 
     * @param toConvert string in fashion (ddMMYYHHmm)
     * @return a date object
     */

    private static Date convertStringtoDate(String toConvert) {
        Calendar c = Calendar.getInstance();

        String tempDayString = toConvert.substring(0, 2);

        int tempDay = Integer.parseInt(tempDayString);

        String tempMonthString = toConvert.substring(2, 4);
        int tempMonth = Integer.parseInt(tempMonthString);

        String tempYearString = toConvert.substring(4, 6);
        int tempYear = Integer.parseInt(tempYearString);

        String tempHourString = toConvert.substring(6, 8);
        int tempHour = Integer.parseInt(tempHourString);

        String tempMinString = toConvert.substring(8, 10);
        int tempMin = Integer.parseInt(tempMinString);

        c.set(tempYear, tempMonth, tempDay, tempHour, tempMin, 0);
        Date toReturn = c.getTime();

        return toReturn;
    }
    
    

    /**
     * delimted a string array in to a string delimted '/n'
     * @param toConvert sting array to be delimted 
     * @return delimted string
     */
    private static String convertArraytoString(String[] toConvert){
        StringBuilder toReturn = new StringBuilder();
        for (String toAdd : toConvert) {
            toReturn.append(toAdd);
            toReturn.append("\n");
        }
        return toReturn.toString();
    }

    public static void addCourseworks(ArrayList<se1.Coursework> courseworks) {
        PreparedStatement stmt = null;
        try {
            for (se1.Coursework corse : courseworks) {
                stmt = c.prepareStatement("INSERT INTO database.event (eventid, title,organizerid, starttime ,endtime, notes, moduleid,typeofobject) VALUES (?,?,?,?,?,?,?,?)");
                stmt.setInt(1, se1.Controllers.DatabaseController.maxEventID() + 1);
                stmt.setString(2, corse.getTitle());
                stmt.setInt(3, corse.getOrgansier().getUniversityID());
                stmt.setInt(4, Integer.parseInt(convertDatetoString((corse.getStartTime()))));
                stmt.setInt(5, Integer.parseInt(convertDatetoString((corse.getStartTime()))));
                stmt.setString(6, corse.getNotes()[0]);
                stmt.setString(7, corse.getModuleID());
                stmt.setInt(8, 3);
                stmt.executeUpdate();
                stmt = c.prepareStatement("INSERT INTO database.milestone(id, title, deadline, notes, eventid) VALUES (?,?,?,?,?)");
                stmt.setInt(1, se1.Controllers.DatabaseController.maxMilestoneID() + 1);
                stmt.setString(2, corse.getTitle());
                String temp = convertDatetoString((corse.getStartTime()));
                String replace = String.format("%10s", temp).replace(' ', '0');
                stmt.setInt(3, Integer.parseInt(replace));
                stmt.setString(4, corse.getNotes()[0]);
                stmt.setInt(5, se1.Controllers.DatabaseController.maxEventID());
                stmt.executeUpdate();
                c.commit();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void addTimeTableEvents(ArrayList<se1.TimetableEvent> timeEvent) {
        PreparedStatement stmt = null;
        try {
            for (se1.TimetableEvent time : timeEvent) {
                stmt = c.prepareStatement("INSERT INTO database.event (eventid, title,organizerid, starttime ,endtime, notes, moduleid,typeofobject) VALUES (?,?,?,?,?,?,?,?)");
                stmt.setInt(1, se1.Controllers.DatabaseController.maxEventID() + 1);
                stmt.setString(2, time.getTitle());
                stmt.setInt(3, time.getOrgansier().getUniversityID());
                stmt.setInt(4, Integer.parseInt(convertDatetoString((time.getStartTime()))));
                stmt.setInt(5, Integer.parseInt(convertDatetoString((time.getStartTime()))));
                stmt.setString(6, "");
                stmt.setString(7, time.getModuleID());
                stmt.setInt(8, 0);
                stmt.executeUpdate();
                stmt = c.prepareStatement("INSERT INTO database.milestone(id, title, deadline, notes, eventid) VALUES (?,?,?,?,?)");
                stmt.setInt(1, se1.Controllers.DatabaseController.maxMilestoneID() + 1);
                stmt.setString(2, time.getTitle());
                String temp = convertDatetoString((time.getStartTime()));
                String replace = String.format("%10s", temp).replace(' ', '0');
                stmt.setInt(3, Integer.parseInt(replace));
                stmt.setString(4, "");
                stmt.setInt(5, se1.Controllers.DatabaseController.maxEventID());
                stmt.executeUpdate();
                c.commit();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void addExams(ArrayList<se1.Exam> exams) {
        PreparedStatement stmt = null;
        try {
            for (se1.Exam exam : exams) {
                stmt = c.prepareStatement("INSERT INTO database.event (eventid, title,organizerid, starttime ,endtime,notes, moduleid, typeofobject) VALUES (?,?,?,?,?,?,?,?)");
                stmt.setInt(1, se1.Controllers.DatabaseController.maxEventID() + 1);
                stmt.setString(2, exam.getTitle());
                stmt.setInt(3, exam.getOrgansier().getUniversityID());
                stmt.setInt(4, Integer.parseInt(convertDatetoString((exam.getStartTime()))));
                stmt.setInt(5, Integer.parseInt(convertDatetoString((exam.getStartTime()))));
                stmt.setString(6, exam.getLocation());
                stmt.setString(7, exam.getModuleID());
                stmt.setInt(8, 4);
                stmt.executeUpdate();
                stmt = c.prepareStatement("INSERT INTO database.milestone(id, title, deadline, notes, eventid) VALUES (?,?,?,?,?)");
                stmt.setInt(1, se1.Controllers.DatabaseController.maxMilestoneID() + 1);
                stmt.setString(2, exam.getTitle());
                String temp = convertDatetoString((exam.getStartTime()));
                String replace = String.format("%10s", temp).replace(' ', '0');
                stmt.setInt(3, Integer.parseInt(replace));
                stmt.setString(4, exam.getNotes()[0]);
                stmt.setInt(5, se1.Controllers.DatabaseController.maxEventID());
                stmt.executeUpdate();
                c.commit();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
