package se1.Controllers;

/**
 *
 * @author SE1
 */
public class LogOnController {

    /**
     * To see if a user has the correct details to log on to the system
     *
     * @param username of profile to be logged on to the system
     * @param password of profile to be logged on to the system
     * @return null if profile not found or valid to be profile found return
     * that profile
     */
    public boolean userAuth(int username, String password) {
        if (DatabaseController.seachUsers(username)) {
            String check = se1.Controllers.EncryptionController.getMD5(password);
            if(check.equals(DatabaseController.getEncryptPassword(username))){
                return true;
            }
        }   
        return false;
    }
/**
 * Loads the log on Frame
 */
    public static void loadFrame() {
        se1.GUI.LogOnFrame temp = new se1.GUI.LogOnFrame();
        temp.setVisible(true);
    }
}
