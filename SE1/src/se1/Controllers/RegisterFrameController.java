package se1.Controllers;

import java.io.File;
import se1.Profile;

/**
 *
 * @author SE1
 */
public class RegisterFrameController {

    /**
     * load registar frame
     */
    public static void loadFrame() {
        se1.GUI.RegisterFrame temp = new se1.GUI.RegisterFrame();
        temp.setVisible(true);
    }
    /**
     * @param uniId
     * @param email
     * @param inputPutFile XML to read from
     * @returns a profile to be loaded in to the database
     */
    public static Profile createProfile(String uniId ,String email,File inputPutFile){
      XMLFileReader reader = new XMLFileReader();
      reader.readXMLFile(inputPutFile);
      Profile toReturn;
      int toAdduniId =  Integer.valueOf(uniId);
      String username = email.split(email)[0];
      toReturn = new Profile(username,toAdduniId,email,reader.storedExams,reader.storedTimeTable,reader.storedCoursework);
      //James make sure this is added to the database
      return toReturn;
    }
    
}
