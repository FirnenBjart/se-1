package se1.Controllers;

import java.util.ArrayList;
import se1.Milestone;

/**
 *
 * @author SE1
 */
public class StudyDashboardController {

    se1.Profile profile;

    public static void loadFrame(int username) {
        se1.GUI.StudyDashboardFrame temp = new se1.GUI.StudyDashboardFrame(username);
        temp.setVisible(true);
    }

    public void loadProfile(int username) {
        profile = DatabaseController.getProfile(username);
        System.out.println(profile.getMilestones().size());
    }

    public int getTotalEvents() {
        return profile.milestoneTotal();
    }
    public int getApporach() {
        return profile.getApporachingMilestones();
    }

    public int getPassed() {
        return profile.getPassedDeadlines();
    }

    public String getTaskTitles() {
        return profile.getName();
    }
    public ArrayList<Milestone> getMilestones(){
        return profile.getMilestones();
    }
}
