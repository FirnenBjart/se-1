package se1.Controllers;

/**
 *
 * @author SE1
 */
public class TaskToBeDoneController {
    public static void loadFrame(se1.Task task) {
        se1.GUI.TaskToBeDoneFrame temp = new se1.GUI.TaskToBeDoneFrame(task);
        temp.setVisible(true);
        temp.setAlwaysOnTop(true);
    }
}
