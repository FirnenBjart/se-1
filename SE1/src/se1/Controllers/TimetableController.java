package se1.Controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SE1
 */
public class TimetableController {
    /**
     * load TimetableFrame
     * @param user get users data ready 
     */
    public static void loadFrame(int user){
        Date date = firstDayOfWeek(new Date());

        se1.GUI.TimetableFrame temp = new se1.GUI.TimetableFrame(user, date);
        temp.setVisible(true);
    }
 
    /**
     * gets the previous dates
     * @param currentDate
     * @return  the perivous date
     */
    public Date getPreviousDates(Date currentDate){
        Calendar cal = Calendar.getInstance();
        Date newDate;
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, -7);
        newDate = cal.getTime();
        return newDate;
    }
    
    /**
     * 
     * @param oldDate the old date
     * @param userID user being analysed ID 
     * @return 2d array of times linked to events
     */
    public Object[][] getPreviousData(Date oldDate, int userID){
        Calendar cal = Calendar.getInstance();
        cal.setTime(oldDate);
        cal.add(Calendar.DAY_OF_MONTH, -7);
        return getTimetableData(cal.getTime(), userID);
    }
    
    /**
     * gets the Next dates
     * @param currentDate
     * @return  the Next date
     */
    public Date getNextDates(Date currentDate){
        Calendar cal = Calendar.getInstance();
        Date newDate;
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, +7);
        newDate = cal.getTime();
        return newDate;
    }
     /**
     * 
     * @param oldDate the old date
     * @param userID user being analysed ID 
     * @return 2d array of times linked to events
     */
    public Object[][] getNextData(Date oldDate, int userID){
        Calendar cal = Calendar.getInstance();
        cal.setTime(oldDate);
        cal.add(Calendar.DAY_OF_MONTH, +7);
        return getTimetableData(cal.getTime(), userID);
    }
    
    
    /**
     * gets data for timetable about a user  
     * @param weekStart when the week starts 
     * @param userID users ID for timetable timetable
     * @return 2d array containing data to make the timetable
     */
    public Object[][] getTimetableData(Date weekStart, int userID){
        Object[][] data = new Object[25][8];
        Calendar cal = Calendar.getInstance();
        cal.setTime(weekStart);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy");
        
        //Gets dates for a week
        for (int i = 1; i < 8; i++){     
            data[0][i] = sdf.format(cal.getTime());
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        cal.add(Calendar.DAY_OF_MONTH, -7);
        
        //Gets the times for a week
        for (int i = 1; i < 24; i++) {
            data[i][0] = String.format("%02d", i - 1) + ":00 - " + String.format("%02d", i) + ":00";
        }
        data[24][0] = "23:00 - 00:00";
        
        se1.Controllers.DatabaseController db = new se1.Controllers.DatabaseController();
        ArrayList<se1.TimetableEvent> tte = se1.Controllers.DatabaseController.getEvent(userID);
        
        cal.set(Calendar.DAY_OF_WEEK, 1);
        
        for(int j = 0; j < tte.size(); j++){
            for(int p = 1; p < 8; p++){
                data[0][p] = sdf.format(cal.getTime());
                cal.add(Calendar.DAY_OF_MONTH, 1);
                for(int q = 1; q < 23; q++){
                    if(tte.get(j).getStartTime().getHours() == q && 
                            tte.get(j).getStartTime().getDate() == cal.getTime().getDate()){
                        data[q][p] = tte.get(j).getTitle();

                    }
                }
            }            
        }
        return data;
    }
    
    /**
     * gets first day of the week 
     * @param date Date being configed
     * @return the configed Date
     */
    private static Date firstDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        cal.clear(Calendar.HOUR);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        return cal.getTime();
    }
}
