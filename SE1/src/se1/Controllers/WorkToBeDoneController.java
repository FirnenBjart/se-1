package se1.Controllers;

/**
 *
 * @author SE1
 */
public class WorkToBeDoneController {
    
    public static void loadFrame(int user) {
        se1.GUI.WorkToBeDoneFrame temp = new se1.GUI.WorkToBeDoneFrame(user);
        temp.setVisible(true);
    }
    public static void loadFrame(se1.Milestone stone) {
        se1.GUI.WorkToBeDoneFrame temp = new se1.GUI.WorkToBeDoneFrame(stone);
        temp.setVisible(true);
    }
    
    
}
