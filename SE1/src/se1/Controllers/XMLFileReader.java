package se1.Controllers;

import java.util.ArrayList;
import se1.Exam;
import se1.TimetableEvent;
import se1.Coursework;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

import java.util.Calendar;
import java.util.Date;
import se1.Milestone;
import se1.Profile;

/**
*Made by SE1
* 
**/
public class XMLFileReader {
    
     public ArrayList<Exam> storedExams; //Temp stored exams from XML file 
     public ArrayList<TimetableEvent> storedTimeTable;//Temp stored TimeTable from XML file 
     public ArrayList<Coursework>storedCoursework;//Temp stored Coursework from XML file 
     
     
     public XMLFileReader(){
         storedExams= new ArrayList<Exam>();
         storedTimeTable = new ArrayList<TimetableEvent>();
         storedCoursework = new  ArrayList<Coursework>();
     }
     
     
        private Profile makeProfile(){
        
        int username = 1;
        Profile toReturn = new Profile(username);
        
        return toReturn;
    }
     
     /**
      * 
      * @param toBreakdown containing data about a date
      * @return a date
      */
     private Date addInDate(String toBreakdown){
          Calendar c = Calendar.getInstance(); 
          String[] temp=toBreakdown.split("\n");
          int tempDay =(Integer.parseInt(temp[1]));
          int tempMonth =(Integer.parseInt(temp[2])); 
          int tempYear = (Integer.parseInt(temp[3])); 
          int tempHour = (Integer.parseInt(temp[4])); 
          int tempMin = (Integer.parseInt(temp[5])); 
          Date toReturn ;
          c.set(tempYear, tempMonth, tempDay, tempHour, tempMin, 0);
          toReturn =c.getTime();
           
          return toReturn;
     }
     
/**
 * Reads a XML file and adds values to the arralists 
 * @param XMLfile file being read 
 * @return true if succesful otherwise false
 */
    public boolean readXMLFile(File XMLfile){
        
        try{
        
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(XMLfile);
        
        NodeList nList = doc.getElementsByTagName("Exam");
        
        //loop though all Exams in XML
        for (int temp = 0; temp < nList.getLength(); temp++) {
            
            Node nNode = nList.item(temp);
             
            Element eElement = (Element) nNode;
                  
            String title  = eElement.getElementsByTagName("TITLE").item(0).getTextContent();
              
            int profileId = Integer.parseInt( eElement.getElementsByTagName("PROFILEID").item(0).getTextContent());
              
            Date startTime = addInDate(eElement.getElementsByTagName("startTime").item(0).getTextContent());
  
            Date endTime = addInDate(eElement.getElementsByTagName("endTime").item(0).getTextContent());
            
            String moduleID =  eElement.getElementsByTagName("moduleID").item(0).getTextContent();
           
            String location  = eElement.getElementsByTagName("location").item(0).getTextContent();
            
            int weight = Integer.valueOf(eElement.getElementsByTagName("weight").item(0).getTextContent()); 
           
            ArrayList<Milestone> milestones = new ArrayList<Milestone>();
          
            NodeList listMilestones = doc.getElementsByTagName("milestones");
               
            for (int temp2 = 0; temp2 < listMilestones.getLength()-1; temp2++){
          
               String mileTitle  = eElement.getElementsByTagName("title").item(0).getTextContent();
             
               int tempDay = Integer.valueOf(eElement.getElementsByTagName("Day").item(0).getTextContent());
            
               int tempMonth = Integer.valueOf(eElement.getElementsByTagName("Month").item(0).getTextContent());
                
               int tempYear = Integer.valueOf(eElement.getElementsByTagName("Year").item(0).getTextContent());
                 
               int tempHour = Integer.valueOf(eElement.getElementsByTagName("Hour").item(0).getTextContent());
                   
               int tempMin = Integer.valueOf(eElement.getElementsByTagName("Min").item(0).getTextContent());
               Calendar c = Calendar.getInstance();
               c.set(tempYear, tempMonth, tempDay, tempHour, tempMin, 0);
               Date tempdeadline=c.getTime();
               int eventID = Integer.valueOf(eElement.getElementsByTagName("eventId").item(0).getTextContent());

               Milestone toAdd = new Milestone(mileTitle,tempdeadline,null,0,eventID);
          
               milestones.add(toAdd);
              
            }    
            Milestone[] toAddMilestones = new  Milestone[milestones.size()];
           
            for( int i = 0; i<milestones.size(); i++){
                toAddMilestones[i]= milestones.get(i);
                 
            }
             
            
             Profile temppp=makeProfile();
            //link database controller so can pull a profile using a profile id
            Exam addExam= new Exam(title,temppp,startTime,endTime ,moduleID,location,weight,toAddMilestones);
                     
            storedExams.add(addExam);
            
        }
         
        NodeList nList2 = doc.getElementsByTagName("TimetableEvent");
          
        //loop though all Exams in XML
        for (int temp = 0; temp < nList2.getLength(); temp++) {
          Node nNode = nList2.item(temp);
           
          Element eElement = (Element) nNode;
           
          String title  = eElement.getElementsByTagName("title").item(0).getTextContent();
        
          int profileId = Integer.parseInt( eElement.getElementsByTagName("PROFILEID").item(0).getTextContent());
             
          Date startTime = addInDate(eElement.getElementsByTagName("startTime").item(0).getTextContent());
              
          Date endTime = addInDate(eElement.getElementsByTagName("endTime").item(0).getTextContent());
              
          String moduleID = eElement.getElementsByTagName("moduleID").item(0).getTextContent();
            
          String location  = eElement.getElementsByTagName("location").item(0).getTextContent();
         
          TimetableEvent toAdd = new TimetableEvent(title,makeProfile(),startTime,endTime,moduleID,location);
          storedTimeTable.add(toAdd);
        }
       
        NodeList nList3 = doc.getElementsByTagName("CourseWork");
          
        //loop though all Exams in XML
        for (int temp = 0; temp < nList3.getLength(); temp++) {
          Node nNode = nList3.item(temp);
         
          Element eElement = (Element) nNode;
          
          String title  = eElement.getElementsByTagName("title").item(0).getTextContent();
          
          int profileId = Integer.parseInt( eElement.getElementsByTagName("OrganiserID").item(0).getTextContent());
          
          Date startTime = addInDate(eElement.getElementsByTagName("startTime").item(0).getTextContent());
            
          Date endTime = addInDate(eElement.getElementsByTagName("endTime").item(0).getTextContent());
          
          String moduleID = eElement.getElementsByTagName("moduleID").item(0).getTextContent();
          
          int weight = Integer.valueOf(eElement.getElementsByTagName("weight").item(0).getTextContent()); 
          
           ArrayList<Milestone> milestones = new ArrayList<Milestone>();
          
            NodeList listMilestones = doc.getElementsByTagName("milestones");
               
            for (int temp2 = 0; temp2 < listMilestones.getLength()-1; temp2++){
          
               String mileTitle  = eElement.getElementsByTagName("title").item(0).getTextContent();
             
               int tempDay = Integer.valueOf(eElement.getElementsByTagName("Day").item(0).getTextContent());
            
               int tempMonth = Integer.valueOf(eElement.getElementsByTagName("Month").item(0).getTextContent());
                
               int tempYear = Integer.valueOf(eElement.getElementsByTagName("Year").item(0).getTextContent());
                 
               int tempHour = Integer.valueOf(eElement.getElementsByTagName("Hour").item(0).getTextContent());
                   
               int tempMin = Integer.valueOf(eElement.getElementsByTagName("Min").item(0).getTextContent());
               Calendar c = Calendar.getInstance();
               c.set(tempYear, tempMonth, tempDay, tempHour, tempMin, 0);
               Date tempdeadline=c.getTime();
               int eventID = Integer.valueOf(eElement.getElementsByTagName("eventId").item(0).getTextContent());

               Milestone toAdd = new Milestone(mileTitle,tempdeadline,null,0,eventID);
          
               milestones.add(toAdd);
             
            }    
            Milestone[] toAddMilestones = new  Milestone[milestones.size()];
           
            for( int i = 0; i<milestones.size(); i++){
                toAddMilestones[i]= milestones.get(i);
            }
            Coursework toAdd = new Coursework(title,makeProfile(),startTime,endTime,moduleID,weight,toAddMilestones);
            storedCoursework.add(toAdd);
        }
       }catch (Exception e){
             System.out.println("Fail");
             return false;
        }
          
        return true;
    }
}
