package se1;
import java.util.Date;

/**
 * Coursework
 * 
 * 
 *
 * @author SE1
 */
public class Coursework extends AcademicEvent{ 
    private boolean extension = false; // does Course work have a extension 
    
    public Coursework(String title, Profile organsier, Date startTime, Date endTime, 
            String notes, String moduleId,double setWeight,Milestone[] setMilestones, Task[] setTask) {
        super(title, organsier, startTime, endTime, notes, moduleId,setWeight,setMilestones,setTask);
    }
 
      public Coursework(String title, Profile organsier, Date startTime, Date endTime, 
             String moduleId,double setWeight,Milestone[] setMilestones) {
        super(title, organsier, startTime, endTime, moduleId,setWeight,setMilestones);
    }

    /**
     * Accessor method to return whether a piece of coursework has had
     * an extension applied to it.
     * 
     * @returns The boolean extension  
     */
    public boolean isExtension() {
        return extension;
    }

    /**
     * Mutator method to set whether a piece of coursework has an extension 
     * or not. 
     * 0 = no extension, 1 = extension applied.
     * 
     * @param extension     Extension attribute to be set.
     */
    public void setExtension(boolean extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return "Coursework{" + "extension=" + extension + ',' + super.toString() + '}';
    }
}
