package se1;
import java.util.Date;
import java.util.ArrayList;

/**
 * Event
 * 
 * An abstract class to represent an event object, which has sub-classes
 * including AcademicEvent and TimetableEvent.
 *
 * @author SE1
 */
public abstract class Event {
    
    private int eventID;//The events Id
    private enum eventType{};//The type of event 
    private String title;// The title of the event
    private Profile organsier; //Profile of organier of the event
    private Date startTime; //start of event 
    private Date endTime; // end of event 
    private String notes; //notes about the event 
    private String moduleID; //the module id of the event 

    public Event(String title, Profile organsier, Date startTime, Date endTime, String setNotes, String moduleId) {
        this.title = title;
        this.organsier = organsier;
        this.startTime = startTime;
        this.endTime = endTime;
        this.notes= setNotes;
        this.moduleID = moduleId;
    } 

    public Event(String title, Profile organsier, Date startTime, Date endTime, String moduleId) {
        this.title = title;
        this.organsier = organsier;
        this.startTime = startTime;
        this.endTime = endTime;
        this.moduleID = moduleId;
    }
    
    public String getTitle() {
        return title;
    }

    public Profile getOrgansier() {
        return organsier;
    }


    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public String getModuleID() {
        return moduleID;
    }
    
    /**
     * Method to add notes to an event object.
     * @param note added to end of notes
     */
    public void addNote(String note){
         notes = (notes + note + "\n");
    }
    
    /**
     * Method to remove a given note from the note container.
     * 
     * @param toRemove removes the certain line form notes 
     * @return true if successful .false if not 
     */
    public boolean removeNote(int toRemove){
        try{
        String[]temp=notes.split("\n");
        ArrayList<String> toRemovee = new ArrayList<String>() ;
        for(String o : temp){
            toRemovee.add(o);
        }
        toRemovee.remove(toRemove);
        StringBuilder tempp = new StringBuilder();
        for(String o : toRemovee){
            tempp.append(o);
        }
        notes= tempp.toString();
        return true;
        }
        catch(Exception e){
            return false;
        }       
    }

    @Override
    public String toString() {
        return "Event{" + "eventID=" + eventID + ", title=" + title + ", organsier=" + organsier + ", startTime=" + startTime + ", endTime=" + endTime + ", notes=" + notes + ", moduleID=" + moduleID + '}';
    }
}

