package se1;
import java.util.Date;

/**
 * Exam
 * 
 * Class to represent an Exam object which extends Academic Event class. 
 * 
 * @author SE1
 */
public class Exam extends AcademicEvent {

    private String location;// location of the event

    public Exam(String title, Profile organsier, Date startTime, Date endTime, String notes, String moduleId, String setLocation,int weight,Milestone[] setMilestones, Task[] setTask) {
        super(title, organsier, startTime, endTime, notes, moduleId,weight,setMilestones,setTask);
        this.location = setLocation;
    }

    public Exam(String title, Profile organsier, Date startTime, Date endTime, String moduleId, String setLocation,int weight,Milestone[] setMilestones ){
        super(title, organsier, startTime, endTime, moduleId,weight,setMilestones);
        this.location = setLocation;
    }
    
    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Exam{" + "location=" + location + ',' + super.toString() + '}';
    }
}

