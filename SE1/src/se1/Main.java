package se1;

import java.io.File;
import java.util.ArrayList;
import se1.Controllers.XMLFileReader;
import se1.GUI.StudyDashboardFrame;

/**
 * Main
 * 
 * Main method to run MyStdentPlanner as a desktop application.
 *
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        se1.Controllers.DatabaseController database = new se1.Controllers.DatabaseController();
        java.awt.EventQueue.invokeLater(() -> {
            new se1.GUI.LogOnFrame().setVisible(true);
        });
        
    }

}
