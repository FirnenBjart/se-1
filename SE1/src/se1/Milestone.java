package se1;

import java.util.ArrayList;
import java.util.Date;
import se1.Controllers.DatabaseController;

/**
 * Milestone
 * 
 * Class to represent a milestone that shows what activities contribute to a 
 * Task.
 * 
 * @author SE1
 */
public class Milestone {

    private String title;//Title of the milestone
    private ArrayList<Task> tasks = new ArrayList();//tasks to do with the milestone
    private Date deadline;//deadline of the milestone
    private String[] notes;//notes about the milestone
    private int eventID; //event it links to
    
    //Types of Milestones
    public enum TaskType {
        LECTURE, EXTRA, COURSEWORK, REVISION, EXAM
    };
    //Type 
    int type;

    public Milestone(String title, Date deadline, String[] notes, int eventID, int type) {
        this.title = title;
        this.deadline = deadline;
        this.notes = notes;
        this.eventID = eventID;
        setTask(this.eventID);
        this.type = type;
    }

    public Milestone(String title, ArrayList<Task> tasks, Date deadline, String[] notes, int eventID, int type) {
        this.title = title;
        this.tasks = tasks;
        this.deadline = deadline;
        this.notes = notes;
        this.eventID = eventID;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public int getType() {
        return type;
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public Date getDeadline() {
        return deadline;
    }

    public String[] getNotes() {
        return notes;
    }

    public int getEventID() {
        return eventID;
    }

    public void setTasks(ArrayList<Task> tasks) {
        this.tasks.addAll(tasks);
    }
    
    /**
     * Methos to set a Task within the database.
     * 
     * @param eventID   Task to be put in the database.
     */
    public void setTask(int eventID) {
        tasks.addAll(DatabaseController.getTasks(eventID));
    }
    
    /**
     * Method to clear and update tasks from the database.
     */
    public void updateTask() {
        tasks.clear();
        setTask(this.eventID);
    }
    
    /**
     * Method to get the task type.
     * 
     * @param i     int type to get.
     * @return      String of what type MileStone it is
     */
    public String taskType(int i) {
        switch (i) {
            case 0:
                return TaskType.LECTURE.toString();
            case 1:
                return TaskType.EXTRA.toString();
            case 2:
                return TaskType.REVISION.toString();
            case 3:
                return TaskType.COURSEWORK.toString();
            case 4:
                return TaskType.EXAM.toString();
        }
        return null;
    }
    
    /**
     * Method to 
     * 
     * @param i
     * @return int get type of milestone
     */
    public static int taskType(String i) {
        switch (i) {
            case "LECTURE":
                return 0;
            case "EXTRA":
                return 1;
            case "REVISION":
                return 2;
            case "COURSEWORK":
                return 3;
            case "EXAM":
                return 4;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Milestone{" + "title=" + title + ", tasks=" + tasks + ", deadline=" + deadline + ", notes=" + notes + ", eventID=" + eventID + '}';
    }

}
