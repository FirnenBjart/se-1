package se1;

/**
 * @author SE1
 */
public class ModuleOrganiser extends Profile{

    public ModuleOrganiser(String name, int UniversityID, String email) {
        super(name, UniversityID, email);   
    }

    @Override
    public String toString() {
        return "ModuleOrganiser{" + ',' + super.toString() + '}';
    }
}
