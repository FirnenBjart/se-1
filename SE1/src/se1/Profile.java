package se1;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import se1.Controllers.DatabaseController;
import java.util.Calendar;

/**
 * @author SE1
 */
public class Profile {


    private String name; //name of profile 
    private int UniversityID; //uni Id
    private String email;//email of the user
    ArrayList<se1.Milestone> milestones = new ArrayList(); //profiles list of milestones 
    ArrayList<se1.Exam>exams= new ArrayList();//profiles list of exams 
    ArrayList<se1.TimetableEvent>timeTable= new ArrayList();//profiles list of timetable events 
    ArrayList<se1.Coursework>courseWork= new ArrayList(); //profiles list of coursework 

    private enum profileType {

    };

    public Profile() {

    }

    public Profile(String name, int UniversityID, String email,
        ArrayList<se1.Exam> setExams, ArrayList<se1.TimetableEvent> settimeTable,
        ArrayList<se1.Coursework> setcourseWork) {
        this.name = name;
        this.UniversityID = UniversityID;
        this.email = email;
        exams = setExams;
        timeTable = settimeTable;
        courseWork = setcourseWork;
        setMilestones();
    }

    public Profile(String name, int UniversityID, String email) {
        this.name = name;
        this.UniversityID = UniversityID;
        this.email = email;
        exams = new ArrayList();
        timeTable = new ArrayList();
        courseWork = new ArrayList();
        setMilestones();
    }

    public Profile(int uniID) {
        Profile temp = se1.Controllers.DatabaseController.fetchProfile(uniID);
        this.name = temp.getName();
        this.email = temp.getEmail();
        this.UniversityID = uniID;
    }

    public String getName() {
        return name;
    }

    public int getUniversityID() {
        return UniversityID;
    }

    public String getEmail() {
        return email;
    }
    /**
    * returns Total number of milestones 
    * @return a int
    */
    public int milestoneTotal() {
        return milestones.size();
    }

    public boolean saveData() {
        throw new Error("TODO");
    }
    
    /**
     * sets the milestones from the database
     */
    public void setMilestones() {
        milestones.addAll(DatabaseController.getMilestones(UniversityID));
    }

    public ArrayList<Milestone> getMilestones() {

        return milestones;
    }

    /**
     * gets number of milestones after todays date
     *
     * @return number of milestones afters todays date
     */
    public int getApporachingMilestones() {
        Calendar today = Calendar.getInstance();
        Date todayy = today.getTime();
        int total = 0;
        for (Milestone mile : milestones) {
            if (todayy.after(mile.getDeadline())) {
                total++;
            }
        }
        return total;
    }

    /**
     * gets number of milestones before todays date
     *
     * @return number of milestones before todays date
     */
    public int getPassedDeadlines() {
        Calendar today = Calendar.getInstance();
        Date todayy = today.getTime();
        int total = 0;
        for (Milestone mile : milestones) {
            if (todayy.before(mile.getDeadline())) {
                total++;
            }
        }
        return total;
    }

    @Override
    public String toString() {
        return "Profile{" + "name=" + name + ", UniversityID=" + UniversityID + ", email=" + email + '}';
    }
}
