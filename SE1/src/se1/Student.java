package se1;

/**
 * Student
 * 
 * This class represents a Student
 *
 * @author SE1
 */
public class Student {
    private String school; //scholl student belong to 
    private Event[] events;// event belonging to student
    private String courseID; // courseID that student is on 
    private Task[] task; // array of tasks student has
    private Milestone[] milestones; // array of milestones student has

    public Student(String school, Event[] events, String courseID, Task[] task, 
            Milestone[] milestones) {
        this.school = school;
        this.events = events;
        this.courseID = courseID;
        this.task = task;
        this.milestones = milestones;
    }

    public String getCourseID() {
        return courseID;
    }

    public Task[] getTask() {
        return task;
    }

    public Milestone[] getMilestones() {
        return milestones;
    }
    

    @Override
    public String toString() {
        return "Student{" + "school=" + school + ", events=" + events + 
                ", courseID=" + courseID + ", task=" + task + ", milestones=" 
                + milestones + '}';
    }
}