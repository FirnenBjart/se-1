package se1;

import java.util.ArrayList;
import se1.Controllers.DatabaseController;

/**
 *
 * @author SE1
 */
public class Task {

    private int taskID;//Tasks Id
    private String title;//Title of the task
    private String notes;//notes about a task 
    ArrayList<se1.Activity> activites = new ArrayList();//list actites linked to a task 
    private boolean isDone;// is Task done 
    
    public Task() {

    }

    public int getTaskID() {
        return taskID;
    }

    public void updateDone() {
        isDone = DatabaseController.updateActiveBool(taskID);
    }

    public Task(int taskID, String title, String notes, boolean isDone) {
        this.taskID = taskID;
        this.title = title;
        this.notes = notes;
        this.isDone = isDone;
        activites.addAll(se1.Controllers.DatabaseController.getActivites(taskID));
    }
    
    
    /**
     * for testing perposes
     */
     public Task(int taskID, String title, String notes, boolean isDone,int toChange) {
        this.taskID = taskID;
        this.title = title;
        this.notes = notes;
        this.isDone = isDone;
     }

    public String getTitle() {
        return title;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }

    public String getNotes() {
        return notes;
    }

    /**
     * Method to add an activity to the database.
     */
    public void setActivities() {
        activites.addAll(se1.Controllers.DatabaseController.getActivites(taskID));
    }

    public boolean isIsDone() {
        return isDone;
    }
    
    /**
     * Method to remove notes from the database.
     * 
     * @param toRemove  Line of which note to remove.
     * @return          A boolean of whether the note has been removed or not,
     *                  true if removed, false otherwise.
     */
   public boolean removeNote(int toRemove){
        try{
        String[]temp=notes.split("\n");
        ArrayList<String> toRemovee = new ArrayList<String>() ;
        for(String o : temp){
            toRemovee.add(o);
        }
        toRemovee.remove(toRemove);
        StringBuilder tempp = new StringBuilder();
        for(String o : toRemovee){
            tempp.append(o);
        }
        notes= tempp.toString();
        return true;
        }
        catch(Exception e){
            return false;
        }
    }
   
   
   /**
    * Method to get all the activities associated with a task.
    */
    public void restActivites() {
        activites.clear();
        activites.addAll(se1.Controllers.DatabaseController.getActivites(taskID));
    }

    public ArrayList<Activity> getActivities() {
        return activites;
    }

    @Override
    public String toString() {
        return "Task{" + "taskID=" + taskID + ", title=" + title + ", notes=" + notes + ", isDone=" + isDone + '}';
    }

}
