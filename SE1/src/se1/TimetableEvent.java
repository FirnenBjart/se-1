package se1;

import java.util.Date;


/**
 * Timetable Event
 * 
 * Class to represent an event that will be available to see on the study 
 * profile. 
 * This class inherits from Event.
 * 
 * @author SE1
 */
public class TimetableEvent extends Event{
    private String location;

   public TimetableEvent(String title, Profile organsier, Date startTime, Date endTime, String notes, 
            String moduleId, String location) {
        super(title, organsier, startTime, endTime, notes, moduleId);
        this.location = location;
    }

    public TimetableEvent(String title, Profile organsier, Date startTime, Date endTime,  
            String moduleId, String location) {
        super(title, organsier, startTime, endTime,  moduleId);
        this.location = location;
    }
   
    public TimetableEvent(String title, Profile organsier, Date startTime, Date endTime,  
            String moduleId) {
        super(title, organsier, startTime, endTime,  moduleId);
    }
    
    
    public String getLocation() {
        return location;
    }
    

    @Override
    public String toString() {
        return "TimetableEvent{" + "location=" + location + '}';
    }
}
