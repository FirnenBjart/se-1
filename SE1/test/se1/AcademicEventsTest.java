
package se1;

import java.util.Date;
import java.sql.Time;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author EdS
 */
public class AcademicEventsTest {
    
    public AcademicEventsTest() {
    }
    
    
   private Milestone makeTestMilestone(){
         
        String title = "TESTtitle";  
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks.add(makeTask());
        Date deadline = new Date(10,10,2013);
        String[] notes = {"Note1","Note2","Note3"}; 
        int id = 984;
        Milestone toReturn= new Milestone(title,tasks,deadline,notes,id,1);
        return toReturn;
    }
    
   
   private Task makeTask(){
        int taskId = 1;
        String title = "qwe" ;
        String notes = "note1 note2 note3";
        boolean isDone = true;
        Activity[] listActivites = {};
        int temp = 123;
        Task toReturn = new Task(taskId,title,notes,isDone,1);
        return toReturn;
    }
    
    /**
     * Method used to generate a set of test data
     * @return a AcademicEvents
     */
   private Coursework makeTestCoursework(){
        String title = "Test";
        Profile pro = new Profile("TestName",123,"TestEmail");
        Time start = new Time(1,1,2013);
        Time endd = new Time(2,1,2013);
        String notes ="TEST";
        int Uni= 897;
        double weight= 10;
        Milestone[] setMilestones= {makeTestMilestone()};
        Task[] setTask= {makeTask()};
        Coursework toReturn = new Coursework(title,pro,start,endd,notes,Uni,weight,setMilestones,setTask);
        
        return toReturn;
    }
   
   private Coursework makeTestCoursework2(){
        String title = "Test2";
        Profile pro = new Profile("TestName2",13,"TestEmail2");
        Time start = new Time(1,8,2013);
        Time endd = new Time(2,7,2013);
        String notes = "TEST2";
        int Uni= 8857;
        double weight= 11;
        Milestone[] setMilestones= {makeTestMilestone()};
        Task[] setTask= {makeTask()};
        Coursework toReturn = new Coursework(title,pro,start,endd,notes,Uni,weight,setMilestones,setTask);
        
        return toReturn;
    }
    

    /**
     * Test of getMilestones method, of class AcademicEvents.
     */
    @Test
    public void testGetMilestones() {
        System.out.println("getMilestones");
        AcademicEvent instance = makeTestCoursework();
        Milestone[] expResult = {makeTestMilestone()};
        Milestone[] result = instance.getMilestones();
       
        System.out.println(result);
        System.out.println(expResult);
        //Manuel inspection needed as stores a array 
       
    }

    /**
     * Test of getTask method, of class AcademicEvents.
     */
    @Test
    public void testGetTask() {
        System.out.println("getTask");
        AcademicEvent instance = makeTestCoursework();
        Task[] expResult = {makeTask()};
        Task[] result = instance.getTask();
        System.out.println(result);
        System.out.println(expResult);
      //Manuel inspection needed as stores a array 
    }

  

  
}
