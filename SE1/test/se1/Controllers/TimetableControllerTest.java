/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1.Controllers;

import java.util.Calendar;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrd15asu
 */
public class TimetableControllerTest {
    
    public TimetableControllerTest() {
    }

   

    /**
     * Test of getPreviousDates method, of class TimetableController.
     */
    @Test
    public void testGetPreviousDates() {
        System.out.println("getPreviousDates");
        
        Date currentDate = new Date(10,10,10);
       

        TimetableController instance = new TimetableController();
        Date exDate = new Date(10,10,3);
        Date result = instance.getPreviousDates(currentDate);
        //Manuel output as 
       assertEquals(exDate, result);
       
        
       
    }

   

    /**
     * Test of getNextDates method, of class TimetableController.
     */
    @Test
    public void testGetNextDates() {
        System.out.println("getNextDates");
        Date currentDate = new Date(10,10,10);
        TimetableController instance = new TimetableController();
        Date exDate = new Date(10,10,17);
        Date result = instance.getNextDates(currentDate);
        assertEquals(exDate, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

}
