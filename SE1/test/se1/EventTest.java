/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1;

import java.util.Date;
import java.sql.Time;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrd15asu
 */
public class EventTest {
    
    public EventTest() {
    }

     
        private Milestone makeTestMilestone(){
         
        String title = "TESTtitle";  
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks.add(makeTask());
        Date deadline = new Date(10,10,2013);
        String[] notes = {"Note1","Note2","Note3"}; 
        int id = 984;
        Milestone toReturn= new Milestone(title,tasks,deadline,notes,id,1);
        return toReturn;
    }
    
    
      private Task makeTask(){
        int taskId = 1;
        String title = "qwe" ;
        String notes = "note1 note2 note3";
        boolean isDone = true;
        Activity[] listActivites = {};
        int temp = 123;
        Task toReturn = new Task(taskId,title,notes,isDone,1);
        return toReturn;
    }
    
    /**
     * Method used to generate a set of test data
     * @return a AcademicEvents
     */
   private Coursework makeTestCoursework(){
        String title = "Test";
        Profile pro = new Profile("TestName",123,"TestEmail");
        Time start = new Time(1,1,2013);
        Time endd = new Time(2,1,2013);
        String notes ="TEST";
        int Uni= 897;
        double weight= 10;
        Milestone[] setMilestones= {makeTestMilestone()};
        Task[] setTask= {makeTask()};
        Coursework toReturn = new Coursework(title,pro,start,endd,notes,Uni,weight,setMilestones,setTask);
        
        return toReturn;
    }
    
    /**
     * Test of getTitle method, of class Event.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Event instance = makeTestCoursework();
        String expResult = "Test";
        String result = instance.getTitle();
        assertEquals("Titles must be the same",expResult, result);
    }

    /**
     * Test of getOrgansier method, of class Event.
     */
    @Test
    public void testGetOrgansier() {
        System.out.println("getOrgansier");
        Event instance = makeTestCoursework();
        Profile expResult = new Profile("TestName",123,"TestEmail");;
        Profile result = instance.getOrgansier();
        
        System.out.println(result);
        System.out.println(expResult);
        //Have to do a manuel inspection
        
    }

    /**
     * Test of getStartTime method, of class Event.
     */
    @Test
    public void testGetStartTime() {
        System.out.println("getStartTime");
        Event instance = makeTestCoursework();
        Time expResult =new Time(1,1,2013);
        Date result = instance.getStartTime();
        assertEquals("Profiles should be the same",expResult, result);
        
       
    }

    /**
     * Test of getEndTime method, of class Event.
     */
    @Test
    public void testGetEndTime() {
        System.out.println("getEndTime");
        Event instance = makeTestCoursework();
        Time expResult  = new Time(2,1,2013);;
        Date result = instance.getEndTime();
        assertEquals("Both tims should be the same" ,expResult, result);
      
    }

    /**
     * Test of getModuleID method, of class Event.
     */
    @Test
    public void testGetModuleID() {
        System.out.println("getModuleID");
        Event instance = makeTestCoursework();
        int expResult = 897;
        int result = instance.getModuleID();
        assertEquals("Both IDs should be the same",expResult, result);
       
      
    }

    /**
     * Test of addNote method, of class Event.
     */
    @Test
    public void testAddNote__RemoveNotes() {
        System.out.println("addNote");
        String note = "NOTE1";
        Event instance = makeTestCoursework();
        instance.addNote(note);
        System.out.println(instance);
         if (!instance.removeNote(0)){
             fail("could not remove notes");
         }
        
    }

 
    public class EventImpl extends Event {

        public EventImpl() {
            super("", null, null, null, null, 0);
        }
    }
    
}
