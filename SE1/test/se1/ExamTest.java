/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrd15asu
 */
public class ExamTest {
    
    public ExamTest() {
    }

    
    private Milestone makeTestMilestone(){
         
        String title = "TESTtitle";  
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks.add(makeTask());
        java.util.Date deadline = new java.util.Date(10,10,2013);
        String[] notes = {"Note1","Note2","Note3"}; 
        int id = 984;
        Milestone toReturn= new Milestone(title,tasks,deadline,notes,id,1);
        return toReturn;
    }
    
    
      private Task makeTask(){
        int taskId = 1;
        String title = "qwe" ;
        String notes = "note1 note2 note3";
        boolean isDone = true;
        Activity[] listActivites = {};
        int temp = 123;
        Task toReturn = new Task(taskId,title,notes,isDone,1);
        return toReturn;
    }
    
    private Exam makeTestExam(){
        String title = "Test";
        Profile pro = new Profile("TestName",123,"TestEmail");
        Time start = new Time(1,1,2013);
        Time endd = new Time(2,1,2013);
        String notes = "NOTE";
        int Uni= 897;
        int weight = 12;
        Milestone[] mile = {makeTestMilestone()};
        Task[] tak = {makeTask()};
        Exam toReturn = new Exam(title,pro,start,endd,notes,Uni,"TestLocation",weight,mile,tak);
       //Makes a AcademicEvent to be tested
        return toReturn;
    }
    
    /**
     * Test of getLocation method, of class Exam.
     */
    @Test
    public void testGetLocation() {
        System.out.println("getLocation");
        Exam instance = makeTestExam();
        String expResult = "TestLocation";
        String result = instance.getLocation();
        System.out.println(result);
        System.out.println(expResult);
        assertEquals("Exam loaction must match",expResult, result);
    }
    
}
