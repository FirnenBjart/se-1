package se1;

import java.util.Date;
import java.sql.Time;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrd15asu
 */
public class MilestoneTest {
    
    public MilestoneTest() {
    }

    
    
     private Milestone makeTestMilestone(){
         
        String title = "TESTtitle";  
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks.add(makeTask());
        java.util.Date deadline = new java.util.Date(10,10,2013);
        String[] notes = {"Note1","Note2","Note3"}; 
        int id = 984;
        Milestone toReturn= new Milestone(title,tasks,deadline,notes,id,1);
        return toReturn;
    }
    
    
      private Task makeTask(){
        int taskId = 1;
        String title = "qwe" ;
        String notes = "note1 note2 note3";
        boolean isDone = true;
        Activity[] listActivites = {};
        int temp = 123;
        Task toReturn = new Task(taskId,title,notes,isDone,1);
        return toReturn;
    }
    
    /**
     * Test of getTitle method, of class Milestone.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Milestone instance = makeTestMilestone();
        String expResult = "TESTtitle";
        String result = instance.getTitle();
        assertEquals("Milestones should match",expResult, result);

    }

    /**
     * Test of getTasks method, of class Milestone.
     */
    @Test
    public void testGetTasks() {
        System.out.println("getTasks");
        Milestone instance = makeTestMilestone();
        Task tasks;
        tasks= makeTask();
        Task result = instance.getTasks().get(0);
        System.out.println(tasks);
        System.out.println(result);
      
       
    }

    /**
     * Test of getDeadline method, of class Milestone.
     */
    @Test
    public void testGetDeadline() {
        System.out.println("getDeadline");
        Milestone instance = makeTestMilestone();
        Date expResult = new Date(10,10,2013);
        Date result = instance.getDeadline();
        assertEquals("Dates must be the same",expResult, result);
      
    }

    /**
     * Test of getNotes method, of class Milestone.
     */
    @Test
    public void testGetNotes() {
        System.out.println("getNotes");
        Milestone instance = makeTestMilestone();
        String[] expResult = {"Note1","Note2","Note3"};
        String[] result = instance.getNotes();
        assertArrayEquals( "Notes must be the same",expResult, result);
       
        
    }

    /**
     * Test of getEventID method, of class Milestone.
     */
    @Test
    public void testGetEventID() {
        System.out.println("getEventID");
        Milestone instance = makeTestMilestone();
        int expResult = 984;
        int result = instance.getEventID();
        assertEquals("EventsId's must be the same",expResult, result);
    }

    
    
}
