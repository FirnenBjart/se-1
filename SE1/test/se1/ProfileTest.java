
package se1;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrd15asu
 */
public class ProfileTest {
    
    public ProfileTest() {
    }

     private Profile makeProfile(){
        
        String name = "TESTname";
        int temp = 123;
        String email = "TestEmail";
        Profile toReturn = new Profile(name,temp,email);
        return toReturn;
    }
    
    /**
     * Test of getName method, of class Profile.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Profile instance = makeProfile();
        String expResult = "TESTname";
        String result = instance.getName();
        assertEquals("Names need to be the same",expResult, result);
     
    }
    
    
    /**
     * Test of getUniversityID method, of class Profile.
     */
    @Test
    public void testGetUniversityID() {
        System.out.println("getUniversityID");
        Profile instance = makeProfile();
        int expResult = 123;
        int result = instance.getUniversityID();
        assertEquals("UniversityID need to be the same",expResult, result);
        
    }

    /**
     * Test of getEmail method, of class Profile.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Profile instance = makeProfile();
        String expResult = "TestEmail";
        String result = instance.getEmail();
        assertEquals("Emails need to be the same",expResult, result);
        
    }
    
}
