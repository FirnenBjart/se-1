/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrd15asu
 */
public class StudentTest {
    
    public StudentTest() {
    }

    
   private Milestone makeTestMilestone(){
         
        String title = "TESTtitle";  
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks.add(makeTask());
        java.util.Date deadline = new java.util.Date(10,10,2013);
        String[] notes = {"Note1","Note2","Note3"}; 
        int id = 984;
        Milestone toReturn= new Milestone(title,tasks,deadline,notes,id,1);
        return toReturn;
    }
    
    private Activity makeTestAcademicEvent(){
        Time time = new Time(10,10,2013);
        Activity toReturn = new Activity("TITLE",30);
        //Makes a AcademicEvent to be tested
        return toReturn;
    }
    
      private Task makeTask(){
        int taskId = 1;
        String title = "qwe" ;
        String notes = "note1 note2 note3";
        boolean isDone = true;
        Activity[] listActivites = {};
        int temp = 123;
        Task toReturn = new Task(taskId,title,notes,isDone,1);
        return toReturn;
    }
    
    /**
     * Method used to generate a set of test data
     * @return a AcademicEvents
     */
   private Coursework makeTestCoursework(){
        String title = "Test";
        Profile pro = new Profile("TestName",123,"TestEmail");
        Time start = new Time(1,1,2013);
        Time endd = new Time(2,1,2013);
        String notes ="TEST";
        int Uni= 897;
        double weight= 10;
        Milestone[] setMilestones= {makeTestMilestone()};
        Task[] setTask= {makeTask()};
        Coursework toReturn = new Coursework(title,pro,start,endd,notes,Uni,weight,setMilestones,setTask);
        
        return toReturn;
    }
    
    private Task makeTestTask(){
        int taskId = 1;
        String title = "qwe";
        String notes = "note1 note2 note3";
        boolean isDone = true;
        Activity[] listActivites = {};
        int temp = 123;
        Task toReturn = new Task(taskId,title,notes,isDone,1);
        return toReturn;
    }

    
     
    
     private Student makeStudent(){
      
        String school = "TestSchool";
        Event[] test ={makeTestCoursework()};
        String corse = "TestCorse";
        Task[] task = {makeTestTask()};
        Milestone[] mile ={makeTestMilestone()};
        Student toReturn= new Student(school,test,corse,task,mile);
        return toReturn;
    }
    
    
    /**
     * Test of getCourseID method, of class Student.
     */
    @Test
    public void testGetCourseID() {
        System.out.println("getCourseID");
        Student instance = makeStudent();
        String expResult = "TestCorse";
        String result = instance.getCourseID();
        assertEquals("CourseID should be equal",expResult, result);
        
    }

    /**
     * Test of getTask method, of class Student.
     */
    @Test
    public void testGetTask() {
        System.out.println("getTask");
        Student instance = makeStudent();
        Task expResult = makeTestTask();
        Task[] result = instance.getTask();
       System.out.println(expResult);
        System.out.println(result[0]);
        
    
    }

    /**
     * Test of getMilestones method, of class Student.
     */
    @Test
    public void testGetMilestones() {
        System.out.println("getMilestones");
        Student instance = makeStudent();
        Milestone expResult = makeTestMilestone();
        Milestone[] result = instance.getMilestones();
        System.out.println(expResult);
        System.out.println(result[0]);
    }

   

    
    
}
