/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1;

import java.sql.Time;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ed.S
 */
public class TaskTest {
    
    public TaskTest() {
        
    }

    
    
     
    
     private Task makeTask(){
        int taskId = 1;
        String title = "qwe" ;
        String notes = "note1 note2 note3";
        boolean isDone = true;
        Activity[] listActivites = {};
        int temp = 123;
        Task toReturn = new Task(taskId,title,notes,isDone,1);
        return toReturn;
    }
     
    /**
     * Test of getTitle method, of class Task.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Task instance = makeTask();
        String expResult = "qwe";
        String result = instance.getTitle();
        assertEquals("Title should be equal",expResult, result);

    }

    /**
     * Test of getNotes method, of class Task.
     */
    @Test
    public void testGetNotes() {
        System.out.println("getNotes");
        Task instance = makeTask();
        String expResult = "note1note2note3";
        String result = instance.getNotes();
        if (expResult.equals(result)){
            fail("Notes should be equal");
        }
    }

    /**
     * Test of isIsDone method, of class Task.
     */
    @Test
    public void testIsIsDone() {
        System.out.println("isIsDone");
        Task instance = makeTask();
        boolean expResult = true;
        boolean result = instance.isIsDone();
        assertEquals("NoIsDonetes should be done true",expResult, result);
       
    }

   
    
}
