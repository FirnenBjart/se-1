/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1;

import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrd15asu
 */
public class TimetableEventTest {
    
    public TimetableEventTest() {
    }
    
    private TimetableEvent makeTimetableEvent(){
        String title = "Test";
        Profile pro = new Profile("TestName",123,"TestEmail");
        Date start = new Date(1,1,2013);
        Date endd = new Date(2,1,2013);
        String notes ="TEST";
        int mod = 123;
        String location ="AHHHH";
        TimetableEvent toReturn = new TimetableEvent(title,pro,start,endd,notes,mod,location);
        return toReturn;
    }
    
    
           
    /**
     * Test of getLocation method, of class TimetableEvent.
     */
    @Test
    public void testGetLocation() {
        System.out.println("getLocation");
        TimetableEvent instance = makeTimetableEvent();
        String expResult = "AHHHH";
        String result = instance.getLocation();
        
        assertEquals("Location must be the same",expResult, result);        
    }

   
    
}
